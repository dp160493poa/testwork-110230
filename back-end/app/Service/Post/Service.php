<?php

namespace App\Service\Post;

use App\Models\Post;
use Illuminate\Support\Facades\DB;

Class Service
{
    public function deletePost($post){
        try{
            DB::beginTransaction();

            $post->delete();

            DB::commit();

        }catch (\Exception $exception){
            DB::rollBack();
            return $exception->getMessage();
        }
    }

    public function storePost($data){
        try{
            DB::beginTransaction();
            $insertData = $data['data'];

            $insertData['author_id'] = auth()->user()->id;
            $post = Post::create($insertData);

            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            return $exception->getMessage();
        }

        return $post;
    }

    public function updatePost($data, $post){
        try {
            DB::beginTransaction();
            $updateData = $data['data'];

            $post->update($updateData);

            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            return $exception->getMessage();
        }

        return $post;
    }
}


