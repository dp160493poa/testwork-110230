import {createRouter, createWebHistory} from 'vue-router'
import {getItem} from "@/helpers/persistanceStorage";

const routes = [
    {
        path: '/',
        name: 'home',
        component: () => import('@/views/HomeView')
    },
    {
        path: '/register',
        name: 'register',
        component: () => import('@/views/Register')
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('@/views/Login')
    },
    {
        path: '/post/:postId',
        name: 'post.show',
        component: () => import('@/views/Post')
    },
    {
        path: '/post/:postId/edit',
        name: 'post.edit',
        component: () => import(/* webpackChunkName: "about" */ '@/views/EditPost')
    },
    {
        path: '/post/new',
        name: 'post.store',
        component: () => import(/* webpackChunkName: "about" */ '@/views/CreatePost')
    },

]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

router.beforeEach((to, from, next) => {
    const accessToken = getItem('access_token')
    console.log(to.name)

    if(accessToken){
        if (to.name === 'login' || to.name === 'register') {
            return next({
                name: 'home'
            })
        }else{
            return next()
        }
    }else{
        if (to.name === 'login' || to.name === 'register'){
            return next()
        }else {
            return next({
                'name': 'login'
            })
        }
    }


})

export default router
