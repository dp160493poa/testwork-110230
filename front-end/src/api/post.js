import api from "@/api/axios";

const getPost = postId => {
    return api.get(`/api/posts/${postId}`)
}

const deletePost = postId => {
    return api.delete(`/api/posts/${postId}`, {} )
}

const createPost = postData => {
    return api.post('/api/posts', {data: postData})
}

const updatePost = (postId, postData) => {
    return api.patch(`/api/posts/${postId}`,  {post: postId, data: postData})
}

export default {
    getPost,
    deletePost,
    createPost,
    updatePost
}