import axios from "axios";
import {getItem} from "@/helpers/persistanceStorage";

axios.defaults.baseURL = 'http://localhost:8089'

const api = axios.create()

api.interceptors.request.use(config => {
    const token = getItem('access_token')
    config.headers.Authorization = token ? `Bearer ${token}` : ''
    return config
},function (error) {
    if (401 === error.response.status) {
        return Promise.reject(error);
    } else {
        return Promise.reject(error);
    }
})

export default api