import post from "@/api/post";

const state = {
    data:null,
    isLoading: false,
    isSubmitting: false,
    validationErrors: null,
    error: null
}

export const mutationTypes = {
    getPostStart: '[gettingPost] Get post start',
    getPostSuccess: '[gettingPost] Get post success',
    getPostFailure: '[gettingPost] Get post failure',

    deletePostStart: '[deletePost] Delete post start',
    deletePostSuccess: '[deletePost] Delete post success',
    deletePostFailure: '[deletePost] Delete post failure',

    updatePostStart: '[updatePost] Edit Post start',
    updatePostSuccess: '[updatePost] Edit Post success',
    updatePostFailure: '[updatePost] Edit Post failure',

    createPostStart: '[createPost] Create Post start',
    createPostSuccess: '[createPost] Create Post success',
    createPostFailure: '[createPost] Create Post failure',
}

export const actionTypes = {
    getPost: '[gettingPost] Get Post',
    deletePost: '[deletePost] Delete Post',
    updatePost: '[updatePost] Update Post',
    createPost: '[createPost] Create Post'
}

const mutations = {
    [mutationTypes.getPostStart](state) {
        state.isLoading = true
        state.data = null
    },
    [mutationTypes.getPostSuccess](state, payload) {
        state.isLoading = false
        state.data = payload
    },
    [mutationTypes.getPostFailure](state) {
        state.isLoading = true
    },

    [mutationTypes.deletePostStart]() {},
    [mutationTypes.deletePostSuccess]() {},
    [mutationTypes.deletePostFailure]() {},

    [mutationTypes.updatePostStart](state) {
        state.isSubmitting = true
    },
    [mutationTypes.updatePostSuccess](state) {
        state.isSubmitting = false
    },
    [mutationTypes.updatePostFailure](state, payload) {
        state.isSubmitting = false
        state.validationErrors = payload
    },

    [mutationTypes.createPostStart](state) {
        state.isSubmitting = true
    },
    [mutationTypes.createPostSuccess](state) {
        state.isSubmitting = false
    },
    [mutationTypes.createPostFailure](state, payload) {
        state.isSubmitting = false
        state.validationErrors = payload
    }
}

const actions = {
    [actionTypes.getPost](context, {postId}) {
        return new Promise(resolve => {
            context.commit(mutationTypes.getPostStart, postId)
            post.getPost(postId)
                .then(res => {
                    context.commit(mutationTypes.getPostSuccess, res.data.data)
                    resolve(res.data.data)
                })
                .catch(() => {
                    context.commit(mutationTypes.getPostFailure)
                })
        })
    },
    [actionTypes.deletePost](context, {postId}) {
        return new Promise(() => {
            context.commit(mutationTypes.deletePostStart, postId)
            post.deletePost(postId)
                .then(() => {
                    context.commit(mutationTypes.deletePostSuccess)
                })
                .catch(() => {
                    context.commit(mutationTypes.deletePostFailure)
                })
        })
    },
    [actionTypes.updatePost](context, {postId, postData}) {
        return new Promise(resolve => {
            context.commit(mutationTypes.updatePostStart)
            post.updatePost(postId, postData).then(post => {
                context.commit(mutationTypes.updatePostSuccess, post)
                resolve(post)
            }).catch(error => {
                context.commit(mutationTypes.updatePostFailure, error)
                resolve(error)
            })
        })
    },
    [actionTypes.createPost](context, {postData}){
        return new Promise(resolve => {
            context.commit(mutationTypes.createPostStart)
            post.createPost(postData).then(post => {
                context.commit(mutationTypes.createPostSuccess, post)
                resolve(post)
            }).catch(error => {
                context.commit(mutationTypes.createPostFailure, error)
                resolve(error)
            })
        })
    }
}

export default {
    state,
    actions,
    mutations
}