<?php

namespace App\Http\Controllers\Post;

use App\Http\Requests\Post\DeleteRequest;
use App\Models\Post;

class DeleteController extends BaseController
{
    public function __invoke(Post $post){

        $this->service->deletePost($post);

        return response([]);
    }
}
