import api from "@/api/axios";

const getFeed = apiUrl => {
    return api.get(apiUrl)
}

export default {
    getFeed
}